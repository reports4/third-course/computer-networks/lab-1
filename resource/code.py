#!/usr/bin/python3

def scrambling(A):
    code = []
    for i in range(len(A)):
        if i < 3:
            b = int(A[i])
        elif i < 5:
            b = int(A[i]) ^ code[i-3]
        else:
            b = int(A[i]) ^ code[i-3] ^ code[i-5]
        code.append(b)
    return ''.join(str(i) for i in code)

A = input('Input: ')
A = ''.join(A.split())
B = scrambling(A)
print('Output:', B)
