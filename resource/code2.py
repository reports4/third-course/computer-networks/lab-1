#!/usr/bin/python3

def scrambling_any_code(code):
    scrambledCode = []
    for i in range(1, len(code) + 1):
        if 1 <= i <= 3:
            b = int(code[i - 1])
        elif 4 <= i <= 5:
            b = int(code[i - 1]) ^ int(scrambledCode[len(scrambledCode) - 5])
        else:
            b = int(code[i - 1]) ^ int(scrambledCode[len(scrambledCode) - 5]) ^ int(scrambledCode[len(scrambledCode) - 7])
        scrambledCode.append(b)
    return scrambledCode

A = input('Input: ')
A = ''.join(A.split())
B = ''.join(str(i) for i in scrambling_any_code(A))
print('Output:', B)
